#!/usr/bin/env python
# -*- coding: latin-1 -*-

import sys
from operator import mul
from math import factorial

def main():
    '''
    '''

    puzzle_input = 2
    
    try:
        sys.argv[1]
    except IndexError:
        user_arg_1 = puzzle_input 
    else:
        user_arg_1 = sys.argv[1]
        try:
            user_arg_1 = int(user_arg_1)
        except:
            #print user_arg_1, "not an int, exiting"
            exit(1)


    print "Lattice Paths for grid", user_arg_1, "x", user_arg_1, "is", (factorial((2*user_arg_1))/factorial(user_arg_1)**2)

    print "slowwwwwwwww method if you didn't google formula and you want to use 'pythonic' functions. Answer:",
    l = [0] * user_arg_1 + [1] * user_arg_1
    import itertools
    #print len(set(itertools.permutations(l)))

if __name__ == "__main__":
    main()
