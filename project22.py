#!/usr/bin/env python
# -*- coding: latin-1 -*-
import sys


def main():
    '''
    Using names.txt (right click and 'Save Link/Target As...'), a 46K text file
    containing over five-thousand first names, begin by sorting it into
    alphabetical order. Then working out the alphabetical value for each name,
    multiply this value by its alphabetical position in the list to obtain a
    name score.
    For example, when the list is sorted into alphabetical order, COLIN, which
    is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So,
    COLIN would obtain a score of 938 × 53 = 49714.
    What is the total of all the name scores in the file?
    '''

    puzzle_input = 10

    try:
        sys.argv[1]
    except IndexError:
        user_arg_1 = puzzle_input
    else:
        user_arg_1 = sys.argv[1]
        try:
            user_arg_1 = int(user_arg_1)
        except:
            print user_arg_1, "not an int, exiting"
            exit(1)

    answer = 0
    import string
    values = dict(zip(string.uppercase, range(1, 27)))

    names = open('names.txt', 'r').readline().split(',')
    names = [name.strip('"') for name in names]
    names.sort()
    for index, name in enumerate(names):
        letter_sum = 0
        for letter in name:
            letter_sum += values[letter]
        answer += (index + 1) * letter_sum
    print answer

if __name__ == "__main__":
    main()
