#!/usr/bin/env python
# -*- coding: latin-1 -*-

import sys
from operator import mul

def main():
    '''
    '''

    puzzle_input = 10
    
    try:
        sys.argv[1]
    except IndexError:
        user_arg_1 = puzzle_input 
    else:
        user_arg_1 = sys.argv[1]
        try:
            user_arg_1 = int(user_arg_1)
        except:
            #print user_arg_1, "not an int, exiting"
            exit(1)

    collatz(user_arg_1)
    
def collatz(user_arg_1=10):
    result = (1,0)
    already_guessed = [0] * 1000000

    def even(n):
        return n/2
    def odd(n):
        return (3*n) + 1

    for guess in range(1, user_arg_1, 1):
        #Begin chain
        length = 1
        iteration = guess
        while iteration > 1:
            try:
                if already_guessed[iteration]:
                    length += already_guessed[iteration]
                    break
            except:
                pass

            if iteration % 2 == 0: 
                iteration = even(iteration)
            else:
                iteration = odd(iteration)

            length +=1

        try:
            already_guessed[guess] = length
        except: pass

        if length > result[1]:
            result = (guess, length)

    print result
    #(837799, 525)
if __name__ == "__main__":
    main()
