#!/usr/bin/env python
# -*- coding: latin-1 -*-

import sys

def main():
    '''
    '''

    puzzle_input = 10
    
    try:
        sys.argv[1]
    except IndexError:
        user_arg_1 = puzzle_input 
    else:
        user_arg_1 = sys.argv[1]
        try:
            user_arg_1 = int(user_arg_1)
        except:
            print user_arg_1, "not an int, exiting"
            exit(1)

    values, file_line = [], 0
    for line in reversed(open('input_18.txt').readlines()):
        values.append(list(map(int, line.split())))
    for line in values:
        file_line += 1
        for index, value in enumerate(line[:-1]):
            #print "added", max(line[index], line[index+1]), "to", values[file_line][index]
            values[file_line][index] += max(line[index], line[index+1])
    print values[file_line - 1]

if __name__ == "__main__":
    main()
