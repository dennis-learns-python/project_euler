""" Returns all the factors of given int. Calculates primes if they are not
passed in."""


from sieve import sieve


def n_prime_factors(n=None, primes=None):
    results = []
    if n is None:
        return results
    if primes is None:
        print "n_prime_factors is calculating primes"
        all_primes = sieve(n)
        primes = [0] * n
        for prime in all_primes:
            primes[prime] = 1

    prime_factors = []
    for each in range(1, n):
        if primes[each] and not n % each:
            prime_factors.append(each)

    def factor_n(i=n):
        for each in prime_factors:
            if i % each == 0:
                results.append(i / each)
                factor_n(i / each)
    factor_n(n)
    return sorted(set(results))
