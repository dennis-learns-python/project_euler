#!/usr/bin/env python
# -*- coding: latin-1 -*-

import sys
from collections import Counter

def main():
    '''
    By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

    What is the 10 001st prime number
    '''
    
    try:
        sys.argv[1]
    except IndexError:
        upper = 10 
    else:
        upper = sys.argv[1]
        try:
            upper = int(upper)
        except:
            print upper, "not an int, exiting"
            exit(1)

    match = 0
    step = 1
    while match < 0: #10001:
        step += 1
        if prime(step):
            match += 1 
            print step,
    #print match, step

    print "GEN_2"
    my_iter = prime_gen_2()
    for each in range(upper):
        print my_iter.next(),

    while True:
        break
        try:
            if not prime(raw_input("Value to check: ")):
                print "Not ",
            print "A Prime"
        except KeyboardInterrupt:
            print
            exit(0) 
        
def prime(guess):
    #return all(guess % i for i in xrange(2, guess))
    for each in xrange(2, int(guess**.5 + 1)):
        if guess % each == 0 and each < guess:
            return False
    return True

def prime_gen(upper): #iter primes from 2 ... upper
    place = 2
    while place < upper:
        for each in xrange(2, int(upper + 1)):
            if place == each: #found no even divsors, must be prime
                yield each

            if place % each == 0:  #found divsor, not prime
                break

        place +=1

def prime_gen_2():
    place = 1
    while True:
        place +=1
        for each in xrange(2, int(place**.5 + 1)):
            if place % each == 0:
                break # not a prime, guess again 
        else:
            yield place

def primes():
    while True:
        for each in xrange(3, 10000, 2):
            if each == number:
                yield each 
                return
            if number % each == 0:
                number /= each  
                yield each 
                break

if __name__ == "__main__":
    main()
