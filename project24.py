#!/usr/bin/env python
# -*- coding: latin-1 -*-
import sys
from math import factorial


def main():
    '''
    A permutation is an ordered arrangement of objects.
    For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4.
    If all of the permutations are listed numerically or alphabetically,
    we call it lexicographic order. The lexicographic permutations of 0, 1
    and 2 are:
    012   021   102   120   201   210
    What is the millionth lexicographic permutation of the digits
    0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
    '''

    try:
        sys.argv[1]
        sys.argv[2]
    except IndexError:
        n, count = 10, 1000000
    else:
        n, count = sys.argv[1], sys.argv[2]
        try:
            n = int(n)
            count = int(count)
        except:
            print "not an int, exiting"
            exit(1)

    def determine_digit(mark=0, goal=0, shift=0):
        if shift == 0:return  # Exit recursive call
        weight = factorial(shift) / shift  # Rotation weight
        for each in range(n):
            if weight * each >= goal - mark:
                lexo.insert(n - shift, lexo.pop(each - 1 + (n - shift)))
                mark += weight * (each - 1)
                break
        determine_digit(mark, goal, shift - 1)

    lexo = range(n)
    determine_digit(goal=count, shift=n)
    print ''.join([str(x) for x in lexo])

if __name__ == "__main__":
    main()
