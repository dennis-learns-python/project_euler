#!/usr/bin/env python

import sys

def main():
    '''
    If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
    Find the sum of all the multiples of 3 or 5 below 1000.
    '''
    total = 0

    try:
        sys.argv[1]
    except IndexError:
        upper = 1000
    else:
        upper = sys.argv[1]
        try:
            upper = int(upper)
        except:
            print upper, "not an int, exiting"
            exit(1)

    for n in range(1,upper):
        if n % 3 == 0 or n % 5 == 0:
            total += n
    print "The sum of all the multiples of 3 or 5 below", upper, "is", total

if __name__ == "__main__":
    main()

#Found solution on euler, more pythonic / better
'''
threeMultiples = set(range(0, 1000, 3))
fiveMultiples = set(range(0, 1000, 5))
multipleUnion = threeMultiples | fiveMultiples
sum = 0
for num in multipleUnion: sum += num print sum
 '''
