#!/usr/bin/env python
# -*- coding: latin-1 -*-

import sys
from collections import Counter

def main():
    '''
    2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
    What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
    2 - 2   2 ** 1  
    3 - 3   3 ** 1  
    4 - 2   2 ** 2  
    5 - 5   5 ** 1 
    6 - 2   2 ** 1 + 3 ** 1  
    7 - 7   7 ** 1  
    8 - 2   2 ** 3  
    9 - 3   3 ** 2  
    10- 2   5 ** 1 + 2 ** 1  
    Largest
    2 ** 3 8
    3 ** 2 9
    5 ** 1 5
    7 ** 1 7
    '''
    
    try:
        sys.argv[1]
    except IndexError:
        upper = 600851475143 
        upper = 10 
    else:
        upper = sys.argv[1]
        try:
            upper = int(upper)
        except:
            print upper, "not an int, exiting"
            exit(1)

    my_dict = {}
    my_list = []
    answer = 1
    for i in range(2, upper + 1):
        del my_list[:]
        for prime in primes(i):
            my_list.append(prime) #load up all the primes factors
        for each in Counter(my_list).iterkeys(): #treat the list as dict
            if not my_dict.has_key(each): #new prime, add it to dict
                my_dict[each] = 0
            if my_dict[each] < Counter(my_list).get(each): #found a larger power 
                my_dict[each] = Counter(my_list).get(each) 

    for key in my_dict.iterkeys():
        answer *= int(key) ** my_dict[key]
    print my_dict, "Answer", answer
    
def primes(number):
    print "Prime factors for:", str(number), "are:",
    while True:
        for each in xrange(2, number + 1):
            if each == number:
                print each
                yield each 
                return
            if number % each == 0:
                print each,
                number /= each  
                yield each 
                break

if __name__ == "__main__":
    main()
