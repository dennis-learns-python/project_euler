#!/usr/bin/env python
# -*- coding: latin-1 -*-
import sys
from math import sqrt as r


def main():
    '''
    The Fibonacci sequence is defined by the recurrence relation:
    Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.
    Hence the first 12 terms will be:
    F1 = 1
    F2 = 1
    F3 = 2
    F4 = 3
    F5 = 5
    F6 = 8
    F7 = 13
    F8 = 21
    F9 = 34
    F10 = 55
    F11 = 89
    F12 = 144
    The 12th term, F12, is the first term to contain three digits.
    What is the first term in the Fibonacci sequence to contain 1000 digits?
    '''

    try:
        sys.argv[1]
    except IndexError:
        goal = 10
    else:
        goal = sys.argv[1]
        try:
            goal = int(goal)
        except:
            print "not an int, exiting"
            exit(1)

    GR = 1.61803398874989484820
    post = 1

    def fib_dig(n=1):

        guess = str(long(fib(n)))
        print "The value of the " + str(n) + " fib num is " + str(guess)
        print "It has a lenght of " + str(len(guess))
        print "The goal is a lenght of " + str(goal)
        return len(guess)

    def fib(n):
        print "Calculate: " + str(n)
        print GR ** n
        return (((GR ** n) - ((1 - GR) ** n)) / r(5))

    def next_fib():
        yield 0
        x, y = 0, 1
        yield y
        while True:
            x, y = y, x + y
            yield y

    x, y, count = next_fib(), 0, -1
    while len(str(y)) < goal:
        y = x.next()
        count += 1
    print str(count) + " len is " + str(len(str(y)))

    exit()

    x = 0
    while post < goal:
        x += 1
        raw_input(post)
        post = fib_dig(x)

if __name__ == "__main__":
    main()
