#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

#file handler
handler = logging.FileHandler('out.log')
handler.setLevel(logging.INFO)

#logging format
formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s'
                              '%(message)s')
handler.setFormatter(formatter)

#add the handlers to the logger
logger.addHandler(handler)


def main():
    '''
    n! means n × (n − 1) × ... × 3 × 2 × 1
    For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
    and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 +
    0 = 27.
    Find the sum of the digits in the number 100!
    '''

    puzzle_input = 10

    try:
        sys.argv[1]
    except IndexError:
        user_arg_1 = puzzle_input
    else:
        user_arg_1 = sys.argv[1]
        try:
            user_arg_1 = int(user_arg_1)
        except:
            print user_arg_1, "not an int, exiting"
            exit(1)

    result = 1
    f = range(1, user_arg_1 + 1)
    for a, b in zip(f[:len(f) / 2], f[len(f):len(f) / 2 - 1:-1]):
        logger.info(str(a) + str(b))
        result *= (a * b)
    if len(f) % 2:
        result *= f[len(f) / 2]
    print result
    print sum(map(int, list(str(result))))

if __name__ == "__main__":
    main()
