#!/usr/bin/env python
# -*- coding: latin-1 -*-

import sys

def main():
    '''
    A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
    Find the largest palindrome made from the product of two 3-digit numbers.
    '''
    
    candidates = set()
    working_upper = 0
    try:
        sys.argv[1]
    except IndexError:
        upper = 2 
    else:
        upper = sys.argv[1]
        try:
            upper = int(upper)
        except:
            print upper, "not an int, exiting"
            exit(1)
    
    upper = int('9' * upper)+1 
    guess = 0 

    for first in range(1, upper, 1):
        for second in range(1, upper, 1):
            product = first * second
            str_product = str(product)
            if str(product) == str(product)[::-1] and product > guess:
                guess = product
                winners = {'first':first, 'second':second, 'product':product}
    print winners['first'], "times", winners['second'], "is", winners['product'] 

if __name__ == "__main__":
    main()
