#Return prime factors of number via Sieve of Eratosthenes


def sieve(upper):
    result = [2]
    master_list = range(3, upper + 1, 2)
    target_index = 0

    for each in master_list:
        if each:  # found prime
            result.append(each)
            for non_prime in range(target_index, len(master_list), each):
                master_list[non_prime] = 0
        target_index += 1
    return result
