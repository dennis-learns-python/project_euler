#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import logging
from factor import n_prime_factors
from sieve import sieve
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

#file handler
handler = logging.FileHandler('out.log')
handler.setLevel(logging.INFO)

#logging format
formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s'
                              '%(message)s')
handler.setFormatter(formatter)

#add the handlers to the logger
logger.addHandler(handler)


def main():
    '''
    Let d(n) be defined as the sum of proper divisors of n (numbers less than
    n which divide evenly into n).
    If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair
    and each of a and b are called amicable numbers.
    For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22,
    44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1,
    2, 4, 71 and 142; so d(284) = 220.
    Evaluate the sum of all the amicable numbers under 10000.
    '''

    puzzle_input = 10000

    try:
        sys.argv[1]
    except IndexError:
        user_arg_1 = puzzle_input
    else:
        user_arg_1 = sys.argv[1]
        try:
            user_arg_1 = int(user_arg_1)
        except:
            print user_arg_1, "not an int, exiting"
            exit(1)
    all_primes = sieve(user_arg_1)
    primes = [0] * user_arg_1
    for prime in all_primes:
        primes[prime] = 1
    print "Done creating sieve"

    sums = [0] * (user_arg_1 + 1)
    answer = 0
    for each in xrange(1, user_arg_1 + 1):
        sums[each] = sum(n_prime_factors(each, primes))
        if each % 1000 == 0:
            print "Done with ", each
    print "done with summing values"
    for index, value in enumerate(sums):
        try:
            if sums[value] == index and index != value:
                print "amicable", index, sums[index]
                answer += (index + sums[index])
                sums[value] = 0
        except IndexError:
            pass
    print answer

if __name__ == "__main__":
    main()
