#!/usr/bin/env python
# -*- coding: latin-1 -*-

import sys

def main():
    '''
    '''

    puzzle_input = 10
    
    try:
        sys.argv[1]
    except IndexError:
        user_arg_1 = puzzle_input 
    else:
        user_arg_1 = sys.argv[1]
        try:
            user_arg_1 = int(user_arg_1)
        except:
            print user_arg_1, "not an int, exiting"
            exit(1)

    lengths = {
                1:'one',
                2:'two',
                3:'three',
                4:'four',
                5:'five',
                6:'six',
                7:'seven',
                8:'eight',
                9:'nine',
                10:'ten',
                11:'eleven',
                12:'twelve',
                13:'thirteen',
                14:'forteen',
                15:'fifteen',
                16:'sixteen',
                17:'seventeen',
                18:'eighteen',
                19:'nineteen',
                20:'twenty',
                30:'thirty',
                40:'fourty',
                50:'fifty',
                60:'sixty',
                70:'seventy',
                80:'eighty',
                90:'ninety',
                1000:'one thousand'
            }
    number = range(1,user_arg_1+1)
    final = 0
    for num in number:
        if num in lengths.keys():
            #final +=  len(lengths[int(str(num))])
            print lengths[int(str(num))]
            continue

        if len(str(num)) > 2:
            #final +=  len(lengths[int(str(num)[0])])
            print lengths[int(str(num)[0])],
            #final +=  len("hundread")
            print "hundread",
            if str(num)[1:] == "00":
                print
                #pass
            elif int(str(num)[1:]) in lengths.keys():
                #final +=  len("and") +  len(lengths[int(str(num)[1:])])
                print "and" + lengths[int(str(num)[1:])],
            else:
                #final +=  len("and") + len(lengths[int(str(num)[1]+'0')])
                print "and" + lengths[int(str(num)[1]+'0')],
                #final +=  len(lengths[int(str(num)[-1])])
                print lengths[int(str(num)[-1])],
            continue

        if len(str(num)) > 1:
            #final +=  len(lengths[int(str(num)[0]+'0')])
            print lengths[int(str(num)[0]+'0')],
            #final +=  len(lengths[int(str(num)[-1])])
            print lengths[int(str(num)[-1])]

    #print final
        
if __name__ == "__main__":
    main()
