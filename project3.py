#!/usr/bin/env python

import sys

def main():
    '''
    The prime factors of 13195 are 5, 7, 13 and 29.
    What is the largest prime factor of the number 600851475143 ?
    '''
    candidates = set()
    working_upper = 0
    try:
        sys.argv[1]
    except IndexError:
        upper = 600851475143
        #upper = 13195 
    else:
        upper = sys.argv[1]
        try:
            upper = int(upper)
        except:
            print upper, "not an int, exiting"
            exit(1)

    working_upper = upper
    done = False

    while done == False:
        for each in xrange(2, working_upper):
            if working_upper % each == 0:
                candidates.add(each)
                working_upper /= each
                break
            if each +1 == working_upper:
                candidates.add(working_upper)
                done = True
                break
            
    print sorted(candidates)

if __name__ == "__main__":
    main()
