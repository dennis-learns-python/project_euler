#!/usr/bin/env python
# -*- coding: latin-1 -*-

import sys
from collections import Counter

def main():
    '''
    A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
    a^2 + b^2 = c^2

    For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

    There exists exactly one Pythagorean triplet for which a + b + c = 1000.
    Find the product abc.
    '''
    
    try:
        sys.argv[1]
    except IndexError:
        upper = 1000 
    else:
        upper = sys.argv[1]
        try:
            upper = int(upper)
        except:
            print upper, "not an int, exiting"
            exit(1)

    my_iter = primitive_pythagorean()
    guess = 0,

    while upper > sum(guess):
        guess = my_iter.next() 
        if upper % sum(guess)  == 0:
            scale = upper / sum(guess)
            answer = [x * (upper / sum(guess)) for x in guess] 
            print str(sorted(guess)) + " scaled %i times becomes %s" % (scale, answer)
            print "Their product is " + str(reduce(lambda x, y: x*y, answer))
            break
    else:
        print "No solution for %i. Last guess was %s" % (upper, str(guess))

def primitive_pythagorean():
    ''' Use Euclid's formula to return a primitive pythagorean triple '''

    my_iter = base()

    while True:
        x, y = my_iter.next()
        x, y = x**2 - y**2, 2*x*y
        yield x, y, int((x**2 + y**2)**.5)

def base(): 
    '''
    Return candites m,n that will generate primitive pythagorean triple
    m > n
    m and n must be relatively prime
    one must be even and the other odd
    '''

    def rel_prime(m, n):
        #found this slick bit of code pointed out in python standard lib, wow
        # http://stackoverflow.com/questions/11175131/code-for-greatest-common-divisor-in-python
        while n:
            m, n = n, m%n
        if m == 1:
            return True
        return False

    m = 2
    n = 1

    while True:
        if m == n:
            m += 1
            n = 1
        # odd plus even is always odd
        # even plus even is always even
        # odd plus odd is always even 
        # Also, at this point m - n will always be >=2 OR satisfy the 1 odd and 1 even condition
        if (m + n) % 2 == 0: #not an odd and even combo
            n += 1
        if rel_prime(m, n):
            yield m,n
        n += 1

if __name__ == "__main__":
    main()
