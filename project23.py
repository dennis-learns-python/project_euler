#!/usr/bin/env python
# -*- coding: latin-1 -*-
import sys
from sieve import sieve
from factor import n_prime_factors


def main():
    '''
    A perfect number is a number for which the sum of its proper divisors is
    exactly equal to the number. For example, the sum of the proper divisors
    of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect
    number.

    A number n is called deficient if the sum of its proper divisors is less
    than n and it is called abundant if this sum exceeds n.

    As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest
    number that can be written as the sum of two abundant numbers is 24. By
    mathematical analysis, it can be shown that all integers greater than
    28123 can be written as the sum of two abundant numbers. However, this
    upper limit cannot be reduced any further by analysis even though it is
    known that the greatest number that cannot be expressed as the sum of two
    abundant numbers is less than this limit.

    Find the sum of all the positive integers which cannot be written as the
    sum of two abundant numbers.

    '''

    puzzle_input = 10

    try:
        sys.argv[1]
    except IndexError:
        user_arg_1 = puzzle_input
    else:
        user_arg_1 = sys.argv[1]
        try:
            user_arg_1 = int(user_arg_1)
        except:
            print user_arg_1, "not an int, exiting"
            exit(1)

    all_primes = sieve(user_arg_1)
    primes = [0] * user_arg_1
    for prime in all_primes:
        primes[prime] = 1
    print "Sieve built"

    '''perfect, deficient, abundant'''
    number_types = ['a'] * 28123

if __name__ == "__main__":
    main()
