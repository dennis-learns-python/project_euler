#!/usr/bin/env python
# -*- coding: latin-1 -*-

import sys


def main():
    '''
    The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
    Find the sum of all the primes below two million.
    '''

    try:
        sys.argv[1]
    except IndexError:
        upper = 10
    else:
        upper = sys.argv[1]
        try:
            upper = int(upper)
        except:
            print upper, "not an int, exiting"
            exit(1)

    my_iter = prime_gen(1)
    guess = 0
    results = []

    while True:
        guess = my_iter.next()
        if guess < upper:
            results.append(guess)
        else:
            print results
            print sum(results)
            break


def prime_gen(start=1):
    if start % 2 == 0:
        start += 1
    place = start
    while True:
        place += 2
        for each in xrange(2, int(place ** .5 + 1)):
            if place % each == 0:
                break  # not a prime, guess again
        else:
            yield place

if __name__ == "__main__":
    main()
