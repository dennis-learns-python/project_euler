#!/usr/bin/env python
# -*- coding: latin-1 -*-

import sys
from collections import Counter

def main():
    '''
    The sum of the squares of the first ten natural numbers is,
    1**2 + 2**2 + ... + 10**2 = 385

    The square of the sum of the first ten natural numbers is,
    (1 + 2 + ... + 10)**2 = 55**2 = 3025

    Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.

    Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
    '''
   
# Don't know how to math
# http://www.fredonia.edu/faculty/math/JonathanCox/math/SumOfSquares/SumOfSquares.html
# http://en.wikipedia.org/wiki/Triangular_number


    try:
        sys.argv[1]
    except IndexError:
        upper = 600851475143 
        upper = 10 
    else:
        upper = sys.argv[1]
        try:
            upper = int(upper)
        except:
            print upper, "not an int, exiting"
            exit(1)

    print (triangular_number(upper))**2 - sum_of_squares(upper)

def sum_of_squares(upper):
    return (upper*(upper+1)*(2*upper+1))/6

def triangular_number(upper):
    return (upper*(upper+1))/2 

if __name__ == "__main__":
    main()
